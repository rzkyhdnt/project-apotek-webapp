package com.lastproject.apotek.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "resep")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Resep {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String date;
    private String resepImage;
    private String resepText;
    private String status = "Menunggu";
    private boolean deleted = false;
}
