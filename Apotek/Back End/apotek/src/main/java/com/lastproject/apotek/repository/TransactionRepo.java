package com.lastproject.apotek.repository;

import java.util.List;

import com.lastproject.apotek.model.Transaction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TransactionRepo extends JpaRepository<Transaction, Integer> {
    @Query(nativeQuery = true, value = "select * from transaction join resep on transaction.resep_id = resep.id join users on transaction.users_id = users.id where resep.status = 'Disetujui' and users.status_id=2")
    public List<Transaction> findMemberTransaction();

    @Query(nativeQuery = true, value = "select * from transaction join resep on transaction.resep_id = resep.id join users on transaction.users_id = users.id where resep.status = 'Disetujui' and users.status_id=3")
    public List<Transaction> findGuestTransaction();

    @Query(nativeQuery = true, value = "select * from transaction where resep_id is not null")
    public List<Transaction> findAllTransaction();
}
