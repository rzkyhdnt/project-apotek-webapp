package com.lastproject.apotek.controller;

import java.io.IOException;

import com.lastproject.apotek.model.Obat;
import com.lastproject.apotek.model.ObatDTO;
import com.lastproject.apotek.model.Resep;
import com.lastproject.apotek.model.Status;
import com.lastproject.apotek.model.Transaction;
import com.lastproject.apotek.model.Users;
import com.lastproject.apotek.service.ModelService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(value = "/save")
public class SaveController {
    @Autowired
    ModelService modelService;

    @PostMapping(value = "/users", consumes = "application/json")
    public void addUsers(@RequestBody Users users) {
        modelService.saveUsers(users);
    }

    @PostMapping(value = "/status", consumes = "application/json")
    public void addStatus(@RequestBody Status status) {
        modelService.saveStatus(status);
    }

    @PostMapping(value = "/transaction", consumes = "application/json")
    public void addTransaction(@RequestBody Transaction transaction) {
        modelService.saveTransaction(transaction);
    }

    @PostMapping(value = "/obats", consumes = "application/json")
    public void addObat(@RequestBody Obat obat) {
        modelService.saveObat(obat);
    }

    @PostMapping(value = "/resep", consumes = "application/json")
    public void addResep(@RequestBody Resep resep) {
        modelService.saveResep(resep);
    }

    @PostMapping(value = "/obat", consumes = {"multipart/form-data", "application/x-www-form-urlencoded", "application/json"})
    public void addObat(@ModelAttribute ObatDTO obatDTO) {
        try {
            modelService.saveObatDTO(obatDTO);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
