package com.lastproject.apotek.repository;

import java.util.ArrayList;

import com.lastproject.apotek.model.Resep;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ResepRepo extends JpaRepository<Resep, Integer> {
    @Query(nativeQuery = true, value = "select * from transaction join resep on transaction.resep_id = resep.id where resep.status=Menunggu")
    ArrayList<Resep> findAllReseps();
}
