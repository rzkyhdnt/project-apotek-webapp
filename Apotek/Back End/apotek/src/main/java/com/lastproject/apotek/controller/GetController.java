package com.lastproject.apotek.controller;

import java.util.ArrayList;
import java.util.Optional;

import com.lastproject.apotek.model.Users;
import com.lastproject.apotek.service.ModelService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lastproject.apotek.model.Obat;
import com.lastproject.apotek.model.Resep;
import com.lastproject.apotek.model.Status;
import com.lastproject.apotek.model.Transaction;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(value = "/get")
public class GetController {
    @Autowired
    ModelService modelService;

    @GetMapping(value = "/users", produces = "application/json")
    public ArrayList<Users> getUsers(){
        return modelService.getUsers();
    }

    @GetMapping(value = "/status", produces = "application/json")
    public ArrayList<Status> getStatus(){
        return modelService.getStatus();
    }

    @GetMapping(value = "/transaction", produces = "application/json")
    public ArrayList<Transaction> getTransaction(){
        return modelService.getTransactions();
    }

    @GetMapping(value = "/transaction/member", produces = "application/json")
    public ArrayList<Transaction> getTransactionMember(){
        return modelService.getTransactionMember();
    }

    @GetMapping(value = "/transaction/nonmember", produces = "application/json")
    public ArrayList<Transaction> getTransactionGuest(){
        return modelService.getTransactionGuest();
    }

    @GetMapping(value = "/obat", produces = "application/json")
    public ArrayList<Obat> getObat(){
        return modelService.getObat();
    }

    @GetMapping(value = "/resep", produces = "application/json")
    public ArrayList<Resep> getResep(){
        return modelService.getResep();
    }

    //Get Detail

    @GetMapping(value = "/obat/{id}", produces = "application/json")
	public Obat detailObat(@PathVariable("id") int id){
        Optional<Obat> list = modelService.getObatById(id);
        Obat obat = list.get();
        return obat;
    }

    @GetMapping(value = "/users/{id}", produces = "application/json")
	public Users detailUsers(@PathVariable("id") int id){
        Optional<Users> list = modelService.getUsersById(id);
        Users users = list.get();
        return users;
    }

    @GetMapping(value = "/transaction/member/{id}", produces = "application/json")
	public Transaction transactionMember(@PathVariable("id") int id){
        Optional<Transaction> list = modelService.getTransactionById(id);
        Transaction transaction = list.get();
        return transaction;
    }

    @GetMapping(value = "/transaction/nonmember/{id}", produces = "application/json")
	public Transaction transactionNonMember(@PathVariable("id") int id){
        Optional<Transaction> list = modelService.getTransactionById(id);
        Transaction transaction = list.get();
        return transaction;
    }

    @GetMapping(value = "/resep/{id}", produces = "application/json")
	public Resep resep(@PathVariable("id") int id){
        Optional<Resep> list = modelService.getResepById(id);
        Resep resep = list.get();
        return resep;
    }
}
