package com.lastproject.apotek.repository;

import com.lastproject.apotek.model.Status;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusRepo extends JpaRepository<Status, Integer> {
    
}
