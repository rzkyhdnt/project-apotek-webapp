package com.lastproject.apotek.repository;

import com.lastproject.apotek.model.Users;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepo extends JpaRepository<Users, Integer> {

}
