package com.lastproject.apotek.model;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ObatDTO {
    private int id;
    private String nama;
    private String jenis;
    private int stock;
    private Long price;
    private String dosis;
    private String desc;
    private List<MultipartFile> picture;
    private boolean deleted = false;
}
