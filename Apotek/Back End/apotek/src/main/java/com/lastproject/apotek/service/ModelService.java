package com.lastproject.apotek.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.lastproject.apotek.model.Obat;
import com.lastproject.apotek.model.ObatDTO;
import com.lastproject.apotek.model.Resep;
import com.lastproject.apotek.model.Status;
import com.lastproject.apotek.model.Transaction;
import com.lastproject.apotek.model.Users;
import com.lastproject.apotek.repository.ObatRepo;
import com.lastproject.apotek.repository.ResepRepo;
import com.lastproject.apotek.repository.StatusRepo;
import com.lastproject.apotek.repository.TransactionRepo;
import com.lastproject.apotek.repository.UsersRepo;
import org.springframework.web.multipart.MultipartFile;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ModelService {
    @Autowired
    private UsersRepo usersRepo;

    @Autowired
    private StatusRepo statusRepo;

    @Autowired
    private TransactionRepo transactionRepo;

    @Autowired
    private ObatRepo obatRepo;

    @Autowired
    private ResepRepo resepRepo;

    // Get All Controller
    @Transactional
    public ArrayList<Users> getUsers() {
        return (ArrayList<Users>) usersRepo.findAll();
    }

    @Transactional
    public ArrayList<Status> getStatus() {
        return (ArrayList<Status>) statusRepo.findAll();
    }

    @Transactional
    public ArrayList<Transaction> getTransactions() {
        return (ArrayList<Transaction>) transactionRepo.findAllTransaction();
    }

    @Transactional
    public ArrayList<Transaction> getTransactionMember() {
        return (ArrayList<Transaction>) transactionRepo.findMemberTransaction();
    }

    @Transactional
    public ArrayList<Transaction> getTransactionGuest() {
        return (ArrayList<Transaction>) transactionRepo.findGuestTransaction();
    }

    @Transactional
    public ArrayList<Obat> getObat() {
        return (ArrayList<Obat>) obatRepo.findAllObat();
    }

    @Transactional
    public ArrayList<Resep> getResep() {
        return (ArrayList<Resep>) resepRepo.findAll();
    }

    // Save Controller
    @Transactional
    public void saveUsers(Users users) {
        usersRepo.save(users);
    }

    @Transactional
    public void saveStatus(Status status) {
        statusRepo.save(status);
    }

    @Transactional
    public void saveTransaction(Transaction transaction) {
        transactionRepo.save(transaction);
    }

    @Transactional
    public void saveObat(Obat obat) {
        obatRepo.save(obat);
    }

    @Transactional
    public void saveObatDTO(ObatDTO obatDTO) throws IOException {
        ModelMapper mapper = new ModelMapper();
        Obat obat = mapper.map(obatDTO, Obat.class);
        List<MultipartFile> image = obatDTO.getPicture();
        String listPicture = "";

        if (image == null) {
            obatRepo.save(obat);
        } else {
            for (MultipartFile file : image) {
                byte[] bytes = file.getBytes();
                Path path = Paths
                        .get("D:\\Last Project\\Apotek\\Front End\\apotek-app\\public\\" + file.getOriginalFilename());
                Files.write(path, bytes);
                listPicture += "/" + file.getOriginalFilename();
            }

            obat.setPicture(listPicture);
            obatRepo.save(obat);
        }

    }

    @Transactional
    public void saveResep(Resep resep) {
        resepRepo.save(resep);
    }

    // Get Id Controller
    @Transactional
    public Optional<Users> getUsersById(Integer id) {
        return usersRepo.findById(id);
    }

    @Transactional
    public Optional<Status> getStatusById(Integer id) {
        return statusRepo.findById(id);
    }

    @Transactional
    public Optional<Transaction> getTransactionById(Integer id) {
        return transactionRepo.findById(id);
    }

    @Transactional
    public Optional<Obat> getObatById(Integer id) {
        return obatRepo.findById(id);
    }

    @Transactional
    public Optional<Resep> getResepById(Integer id) {
        return resepRepo.findById(id);
    }

    // Delete Controller
    @Transactional
    public void deleteUsers(Integer id) {
        usersRepo.deleteById(id);
    }

    @Transactional
    public void deleteStatus(Integer id) {
        statusRepo.deleteById(id);
    }

    @Transactional
    public void deleteTransaction(Integer id) {
        transactionRepo.deleteById(id);
    }

    @Transactional
    public void deleteObat(Integer id) {
        obatRepo.deleteObat(id);
    }

    @Transactional
    public void deleteResep(Integer id) {
        resepRepo.deleteById(id);
    }
}
