package com.lastproject.apotek.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "transaction")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int quantity;
    private String date;
    private Long price;
    private Long deliverPrice;
    private Long totalPrice;
    private boolean deleted = false;

    @ManyToOne
    @JoinColumn(name="users_id")
    private Users users;

    @OneToOne
    @Cascade(CascadeType.ALL)
    private Resep resep;

    @ManyToMany
    private List<Obat> obat;

}
